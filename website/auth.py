import datetime
from flask import Blueprint, render_template, request, jsonify
from flask_jwt import jwt
from . import db   ##means from __init__.py import db
from . import config
from .models import User
from functools import wraps


auth = Blueprint('auth', __name__)


def jwt_required(f):
    @wraps(f)
    def decorated(*args,**kwargs):
        print("-------------------")
        token = None
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
            print("3333333333333")
            print(token)
        if not token:
            return jsonify({'message':'Token is missing !'}),403
        try:
            # decoding jwt token 
            data = jwt.decode(token,config.SECRET_KEY)
            print("44444444")
            print(data)
        except:
            return jsonify(
                {
                    'message':'Token is Invalid'
                }
            ),403
        return f(data['user_id'],*args,**kwargs)
    return decorated

@auth.route('/login', methods=['GET', 'POST'])
def login():
 
    if request.method =='POST':
        email = request.form['email']
        password = request.form['password']
        user = db.session.query(User).filter(User.email==email).first()
        if user:
            if not password == user.password:
                return jsonify({'message':'Wrong Password !'})
            token = jwt.encode({'user':email,'user_id':user.id,'exp':datetime.datetime.utcnow()+datetime.timedelta(seconds=30)},config.SECRET_KEY)
            
            return jsonify({'access_token':token.decode('UTF-8')})
        else:
            return jsonify({'message':'Somthing went wrong'})
            
    return render_template('login.html')

@auth.route('/sign-up', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        last_name = request.form['last_name']
        first_name = request.form['first_name']
        user = db.session.query(User).filter(User.first_name==first_name).first()
        if not user:
            new_user = {
                'first_name':first_name,
                'last_name':last_name,
                'email':email,
                'password':password
            }
            new_user = User(**new_user)
        db.session.add(new_user)
        db.session.commit()

    return render_template('signup.html')


@auth.route('/home',methods=['GET'])
@jwt_required
def home(current_user):
    user = db.session.query(User).get(current_user)
    return f"Hi You Are authenticated {user.first_name}" 

