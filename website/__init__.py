from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from . import config

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = config.SECRET_KEY
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{config.DB_NAME}'
    db.init_app(app)

    from . import models
    from .views import views
    from .auth import auth

    app.register_blueprint(views,url_prefix="/app")
    app.register_blueprint(auth,url_prefix="/")

    
    if not path.exists('website/'+config.DB_NAME):
        with app.app_context():
            db.create_all()

    return app